Navigate to project location and input below commands in terminal
-----------------------------------------------------------------

Features and corresponding integration tests
--------------------------------------------
1. An endpoint to create a person with basic details like the first and last name, date of birth
   and address details. Address details should have address line 1 and 2, city, post code and
   country
```./mvnw clean test -Dtest=CreatePersonTest```

2. An endpoint to retrieve the person’s details (plus record ID), including all addresses. Search
   for person should be performed using his first and last name
   ```./mvnw clean test -Dtest=GetPersonByFirstAndLastNameTest ```

3. An endpoint to add an additional address record to an existing person, given the person’s
   record identifier
```./mvnw clean test -Dtest=UpdateExistingPersonRecordTest ```

4. An endpoint to delete a person and all related address data from the database
```./mvnw clean test -Dtest=DeletePersonByIdTest ```

5. An endpoint to return a list of persons in the database matching a search based on last name
```./mvnw clean test -Dtest=GetPersonListByLastNameTest ```

6. DOB and mandatory field validation
```./mvnw clean test -Dtest=CreatePersonValidationsTest ```


To run application from terminal
--------------------------------
1. ```./mvnw spring-boot:run```
2. This starts an application on port 8080
3. In memory db can be accessed via http://localhost:8080/h2-console. user details are "sa/password"
4. Swagger UI can be accessed via http://localhost:8080/swagger-ui/#/


To run application in intellij
------------------------------
1. Using intellij create new project from existing source and select project external model as "maven"  
2. Once import is complete, you can run the application by running "Application" class.


Following curl scripts might be useful
--------------------------------------
1. Create a person:
   ``` curl --location --request POST 'localhost:8080/persons' --header 'Content-Type: application/json' --data-raw '{  "firstName" : "Phani",  "lastName" : "Yallam",  "address": [{    "addressLine1": "7",    "addressLine2": "james bond street",    "city": "London",    "postcode": "IG2 6UU",    "country": "UK"  },    { "addressLine1": "16", "addressLine2": "second line", "city": "London", "postcode": "IG3 7UU", "country": "UK"    }  ]}'  ```

2. Get person list by first and last name:
   ``` curl --location --request GET 'localhost:8080/persons?lastName=Yallam&firstName=Phani' --header 'Content-Type: application/json' ```

3. Get person list by last name:
   ``` curl --location --request GET 'localhost:8080/persons?lastName=Yallam' --header 'Content-Type: application/json' ```

4. Update person with new address:
   ``` curl --location --request PUT 'localhost:8080/persons/1/addressList' --header 'Content-Type: application/json' --data-raw '{  "addressLine1": "line1",  "addressLine2": "line2",  "city": "altrincham",  "postcode": "WQ11 3AN",  "country": "UK"}' ```

5. delete a person:
   ``` curl --location --request DELETE 'localhost:8080/persons?id=1' --header 'Content-Type: application/json' ```
