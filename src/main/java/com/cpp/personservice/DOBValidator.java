package com.cpp.personservice;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class DOBValidator implements ConstraintValidator<DOBConstraint, LocalDate> {

  @Override
  public void initialize(DOBConstraint dobConstraint) {}

  @Override
  public boolean isValid(LocalDate dob, ConstraintValidatorContext cxt) {
    if(dob==null){
      return true;
    }
    LocalDate now = LocalDate.now();
    return dob.isBefore(now) || dob.isEqual(now);
  }
}
