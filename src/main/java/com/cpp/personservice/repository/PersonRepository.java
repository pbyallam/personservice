package com.cpp.personservice.repository;

import com.cpp.personservice.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends JpaRepository<Person, Integer> {
  List<Person> findByLastName(String lastName);

  List<Person> findByFirstNameAndLastName(String firstName, String lastName);
}
