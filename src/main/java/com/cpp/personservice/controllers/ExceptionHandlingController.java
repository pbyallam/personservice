package com.cpp.personservice.controllers;

import com.cpp.personservice.model.FieldValidationError;
import com.cpp.personservice.model.ValidationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@ApiIgnore
@ControllerAdvice
public class ExceptionHandlingController {

  @ExceptionHandler({ValidationException.class})
  public ResponseEntity<FieldValidationError> handleValidationException(
      ValidationException ex, WebRequest request) {
    return new ResponseEntity<>(
        ex.getFieldValidationError(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler({ConstraintViolationException.class})
  public ResponseEntity<Set<FieldValidationError>> handleConstraintViolationException(
      ConstraintViolationException exception, ServletWebRequest request) throws IOException {
    Set<ConstraintViolation<?>> constraintViolations = exception.getConstraintViolations();

    Set<FieldValidationError> messages = new HashSet<>(constraintViolations.size());

    messages.addAll(
        constraintViolations.stream()
            .map(
                constraintViolation ->
                    new FieldValidationError(
                        constraintViolation.getPropertyPath().toString(),
                        constraintViolation.getInvalidValue() == null
                            ? ""
                            : constraintViolation.getInvalidValue().toString(),
                        constraintViolation.getMessage()))
            .collect(Collectors.toList()));

    return new ResponseEntity<>(messages, HttpStatus.BAD_REQUEST);
  }
}
