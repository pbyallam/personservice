package com.cpp.personservice.controllers;

import com.cpp.personservice.model.Address;
import com.cpp.personservice.model.Person;
import com.cpp.personservice.service.PersonService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/persons")
public class PersonController {

  private final PersonService personService;

  @Autowired
  public PersonController(PersonService personService) {
    this.personService = personService;
  }

  @PostMapping
  public ResponseEntity<Integer> createPerson(@RequestBody Person person) {

    int newPersonId = personService.create(person);
    return new ResponseEntity<>(newPersonId, HttpStatus.CREATED);
  }

  @GetMapping
  public List<Person> getPersonListBy(
          @RequestParam Optional<String> firstName, @RequestParam String lastName) {

    if (firstName.isPresent() && StringUtils.isNotBlank(lastName)) {
      return personService.getPersonListByFirstAndLastName(firstName.get(), lastName);
    } else if (firstName.isEmpty() && StringUtils.isNotBlank(lastName)) {
      return personService.getPersonListByLastName(lastName);
    } else {
      return null;
    }
  }

  @DeleteMapping
  public void deletePersonRecordById(@RequestParam int id) {
    personService.deletePersonRecordById(id);
  }

  @PutMapping("/{id}/addressList")
  public void updatePersonRecordWithAdditionalAddress(@PathVariable int id, @RequestBody Address address) {
    personService.updateExistingPersonWithAdditionalAddress(id, address);
  }

}
