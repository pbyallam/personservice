package com.cpp.personservice.service;

import com.cpp.personservice.model.Address;
import com.cpp.personservice.model.Person;
import com.cpp.personservice.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonService {

  private final PersonRepository personRepository;

  @Autowired
  public PersonService(PersonRepository personRepository) {
    this.personRepository = personRepository;
  }

  public int create(Person person) {
    return personRepository.save(person).getId();
  }

  public List<Person> getPersonListByLastName(String lastName) {
    return personRepository.findByLastName(lastName);
  }

  public List<Person> getPersonListByFirstAndLastName(String firstName, String lastName) {
    return personRepository.findByFirstNameAndLastName(firstName, lastName);
  }

  public void deletePersonRecordById(int id) {
    personRepository.deleteById(id);
  }

  public void updateExistingPersonWithAdditionalAddress(int id, Address address) {
    Optional<Person> personById = personRepository.findById(id);
    if (personById.isPresent()) {
      Person person = personById.get();
      List<Address> addressList = person.getAddress();
      addressList.add(address);
      personRepository.save(person);
    }
  }
}
