package com.cpp.personservice.model;

public class ValidationException extends Exception{

    private final FieldValidationError fieldValidationError;

    public ValidationException(FieldValidationError fieldValidationError) {
        super();
        this.fieldValidationError = fieldValidationError;
    }

    public FieldValidationError getFieldValidationError() {
        return fieldValidationError;
    }
}
