package com.cpp.personservice.model;

import com.cpp.personservice.DOBConstraint;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "PERSON_TBL")
@JsonPropertyOrder({"id", "firstName","lastName", "dob", "address" })
public class Person {
  @Id
  @GeneratedValue(strategy= GenerationType.IDENTITY)
  @Column private int id;
  @NotNull
  private String firstName;
  @NotNull
  private String lastName;

  @Column(columnDefinition = "DATE")
  @JsonFormat(pattern = "dd/MM/yyyy")
  @DOBConstraint
  private LocalDate dob;

  @OneToMany(cascade = CascadeType.ALL)
  @JsonProperty("address")
  private List<Address> address;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String name) {
    this.firstName = name;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public LocalDate getDob() {
    return dob;
  }

  public void setDob(LocalDate dob) {
    this.dob = dob;
  }

  public List<Address> getAddress() {
    return address;
  }

  public void setAddress(List<Address> address) {
    this.address = address;
  }
}
