package com.cpp.personservice.model;

public class FieldValidationError {

    private final String fieldName;
    private final String fieldValue;
    private final String errorMessage;

    public FieldValidationError(String fieldName, String fieldValue, String errorMessage) {
        super();
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        this.errorMessage = errorMessage;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}