package com.cpp.personservice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class DOBValidatorTest {

  private DOBValidator subject;

  @BeforeEach
  public void init() {
    subject = new DOBValidator();
  }

  @Test
  void validationShouldFailIfDOBIsInFuture() {
    LocalDate futureDate = LocalDate.now().plusYears(1L);
    assertFalse(subject.isValid(futureDate, null));
  }

  @Test
  void validationShouldPassIfDOBIsInPast() {
    LocalDate pastDate = LocalDate.now().minusYears(1L);
    assertTrue(subject.isValid(pastDate, null));
  }

  @Test
  void validationShouldPassIfDOBIsToday() {
    LocalDate today = LocalDate.now();
    assertTrue(subject.isValid(today, null));
  }
  @Test
  void validationShouldPassIfDOBIsNull() {
    LocalDate today = LocalDate.now();
    assertTrue(subject.isValid(null, null));
  }

}
