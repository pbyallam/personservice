package com.cpp.personservice.integrationtests.utils;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Service
public class ITUtils {

    @Autowired
    private MockMvc mvc;
    public MvcResult createPerson(Resource personStubWithMultiAddress) throws Exception {

        String personJson = getResourceText(personStubWithMultiAddress);

        MvcResult mvcResult =
                mvc.perform(
                                MockMvcRequestBuilders.post("/persons")
                                        .content(personJson)
                                        .contentType(MediaType.APPLICATION_JSON))
                        .andReturn();
        return mvcResult;
    }


    public String getResourceText(Resource expectedPersonsResource) throws IOException {
        return FileUtils.readFileToString(expectedPersonsResource.getFile(), StandardCharsets.UTF_8);
    }

}
