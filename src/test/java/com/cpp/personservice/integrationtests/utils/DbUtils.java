package com.cpp.personservice.integrationtests.utils;

import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

@Service
public class DbUtils {
    private static final String JDBC_DRIVER = "org.h2.Driver";
    private static final String DB_URL = "jdbc:h2:mem:persontestdb";
    private static final String DB_USERNAME = "sa";
    private static final String DB_PASSWORD = "password";
    private Connection connection = null;

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(JDBC_DRIVER);
    if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        }
        return connection;
    }

    private void closeConnection() throws SQLException {
        if(connection != null){
            connection.close();
        }
    }

    private void clearDatabase() throws SQLException, ClassNotFoundException {
        try (Statement stmt = getConnection().createStatement()) {
            stmt.execute("DELETE FROM  PERSON_TBL_ADDRESS;");
            stmt.execute("DELETE FROM PERSON_TBL;");
            stmt.execute("DELETE FROM ADDRESS_TBL;");
        }

    }

    public void cleanup() throws SQLException, ClassNotFoundException {
        clearDatabase();
        closeConnection();
    }
}
