package com.cpp.personservice.integrationtests;

import com.cpp.personservice.integrationtests.utils.ITUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;

import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UpdateExistingPersonRecordTest {

  private static final String PERSONS_LIST = "/persons";

  @Autowired private MockMvc mvc;
  @Autowired private ITUtils itUtils;

  @Value("classpath:person_with_multi_address.json")
  private Resource personStub;

  @Value("classpath:expected_person_with_added_address.json")
  private Resource expectedPersonWithAddedAddress;

  @Value("classpath:address.json")
  private Resource additionalAddress;

  private String personId;
  private String newAddressJson;
  private MvcResult personAddressUpdateResult;

  @Test
  void shouldBeAbleToUpdateExistingPersonRecord() throws Exception {

    givenAPersonExists();
    givenANewAddressIsAvailable();

    whenPersonIsUpdatedWithNewAddress();

    thenResponseCodeShouldBe200();
    thenNewAddressShouldBeAddedToPerson();
  }

  private void thenNewAddressShouldBeAddedToPerson() throws Exception {
    MvcResult personResponseWithAddressResult =
        mvc.perform(
                MockMvcRequestBuilders.get(PERSONS_LIST)
                    .queryParam("firstName", "Phani")
                    .queryParam("lastName", "Yallam")
                    .contentType(MediaType.APPLICATION_JSON))
            .andReturn();
    String personResponseWithAddressAdded =
        personResponseWithAddressResult.getResponse().getContentAsString();
    assertThatJson(personResponseWithAddressAdded)
        .isEqualTo(itUtils.getResourceText(expectedPersonWithAddedAddress));
  }

  private void thenResponseCodeShouldBe200() {
    assertEquals(HttpStatus.OK.value(), personAddressUpdateResult.getResponse().getStatus());
  }

  private void whenPersonIsUpdatedWithNewAddress() throws Exception {
    personAddressUpdateResult =
        mvc.perform(
                MockMvcRequestBuilders.put("/persons/" + personId + "/addressList")
                    .content(newAddressJson)
                    .contentType(MediaType.APPLICATION_JSON))
            .andReturn();
  }

  private void givenANewAddressIsAvailable() throws IOException {
    newAddressJson = itUtils.getResourceText(additionalAddress);
  }

  private void givenAPersonExists() throws Exception {
    MvcResult personResultBeforeAddressUpdate = itUtils.createPerson(personStub);
    personId = personResultBeforeAddressUpdate.getResponse().getContentAsString();
  }

}
