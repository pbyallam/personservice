package com.cpp.personservice.integrationtests;

import com.cpp.personservice.integrationtests.utils.DbUtils;
import com.cpp.personservice.integrationtests.utils.ITUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;

import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;

import static java.time.Month.JANUARY;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreatePersonTest {
  @Autowired private ITUtils itUtils;
  @Autowired private DbUtils dbUtils;

  @Value("classpath:person_with_multi_address.json")
  private Resource personStubWithMultiAddress;

  private MockHttpServletResponse response;

  @Test
  void shouldBeAbleToCreateAPerson() throws Exception {
    whenAPersonIsCreated();
    thenResponseShouldHavePersonIdAndStatusShouldBe201();
    thenPersonShouldBeSavedInDB();
  }

  private void thenResponseShouldHavePersonIdAndStatusShouldBe201()
      throws UnsupportedEncodingException {
    assertEquals(HttpStatus.CREATED.value(), response.getStatus());
    String id = response.getContentAsString();
    assertFalse(id.isEmpty());
  }


  private void thenPersonShouldBeSavedInDB() throws UnsupportedEncodingException {
    String id = response.getContentAsString();
    assertPersonDBRecord(id);
  }

  private void whenAPersonIsCreated() throws Exception {
    MvcResult mvcResult = itUtils.createPerson(personStubWithMultiAddress);
    response = mvcResult.getResponse();
  }

  private void assertPersonDBRecord(String id) {
    try (Statement stmt = dbUtils.getConnection().createStatement()) {
      String sql =
          "select PERSON_TBL.first_name, PERSON_TBL.last_name, PERSON_TBL.dob, "
              + "Address_TBL.address_line1, Address_TBL.address_line2, Address_TBL.city, Address_TBL.postcode, Address_TBL.country "
              + "from PERSON_TBL_ADDRESS "
              + "INNER JOIN PERSON_TBL  ON PERSON_TBL_ADDRESS.person_id=PERSON_TBL.id "
              + "INNER JOIN Address_TBL  ON PERSON_TBL_ADDRESS.Address_id=ADDRESS_TBL .id "
              + "where PERSON_TBL.id="
              + id;

      ResultSet rs = stmt.executeQuery(sql);
      assertTrue(rs.isBeforeFirst(), "no records found, but expected a record based on id:" + id);
      int counter = 0;
      while (rs.next()) {
        counter++;
        assertEquals("Phani", rs.getString("first_name"));
        assertEquals("Yallam", rs.getString("last_name"));
        LocalDateTime dob = rs.getTimestamp("dob").toLocalDateTime();
        assertEquals(1980, dob.getYear());
        assertEquals(JANUARY, dob.getMonth());
        assertEquals(1, dob.getDayOfMonth());

        if (counter == 1) {
          assertEquals("7", rs.getString("address_Line1"));
          assertEquals("james bond street", rs.getString("address_Line2"));
          assertEquals("London", rs.getString("city"));
          assertEquals("IG2 6UU", rs.getString("postcode"));
          assertEquals("UK", rs.getString("country"));
        }
        if (counter == 2) {
          assertEquals("16", rs.getString("address_Line1"));
          assertEquals("second line", rs.getString("address_Line2"));
          assertEquals("London", rs.getString("city"));
          assertEquals("IG3 7UU", rs.getString("postcode"));
          assertEquals("UK", rs.getString("country"));
        }
      }
      assertEquals(2, counter);
    } catch (Exception se) {
      se.printStackTrace();
      fail("unable to retrieve data from db");
    }
  }

  @AfterAll
  void cleanup() throws SQLException, ClassNotFoundException {
    dbUtils.cleanup();
  }
}
