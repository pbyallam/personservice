package com.cpp.personservice.integrationtests;

import com.cpp.personservice.integrationtests.utils.DbUtils;
import com.cpp.personservice.integrationtests.utils.ITUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GetPersonByFirstAndLastNameTest {
  private static final String PERSONS_LIST = "/persons";
  @Autowired private MockMvc mvc;
  @Autowired private ITUtils itUtils;
  @Autowired private DbUtils dbUtils;

  @Value("classpath:person_with_multi_address.json")
  private Resource personStub;

  @Value("classpath:person_with_multi_address_response.json")
  private Resource expectedPersonResource;

  private MvcResult mvcResult;
  private MockHttpServletResponse response;

  @Test
  void shouldBeAbleToReturnPersonByFirstAndLastName() throws Exception {
    givenAPersonExists();
    whenThatPersonIsRetrievedByFirstAndLastName();
    thenResponseCodeShouldBe200();
    thenResponseJsonShouldMatchToTheExistingPerson();
  }

  @Test
  void shouldReturnEmptyArrayIfNoMatchFoundForFirstAndLastName() throws Exception {
    whenANonExistingPersonIsRetrievedByFirstAndLastName();
    thenResponseCodeShouldBe200();
    thenResponseJsonShouldBeEmptyArray();
  }

  private void thenResponseJsonShouldBeEmptyArray() throws UnsupportedEncodingException {
    assertThatJson(mvcResult.getResponse().getContentAsString()).isEqualTo("[]");
  }

  private void whenANonExistingPersonIsRetrievedByFirstAndLastName() throws Exception {
    mvcResult =
        mvc.perform(
                MockMvcRequestBuilders.get(PERSONS_LIST)
                    .queryParam("firstName", "NON_EXISTING_FIRST_NAME")
                    .queryParam("lastName", "NON_EXISTING_LAST_NAME")
                    .contentType(MediaType.APPLICATION_JSON))
            .andReturn();
    response = mvcResult.getResponse();
  }

  private void thenResponseJsonShouldMatchToTheExistingPerson() throws IOException {
    assertThatJson(mvcResult.getResponse().getContentAsString())
        .isEqualTo(itUtils.getResourceText(expectedPersonResource));
  }

  private void thenResponseCodeShouldBe200() {
    assertEquals(HttpStatus.OK.value(), response.getStatus());
  }

  private void whenThatPersonIsRetrievedByFirstAndLastName() throws Exception {
    mvcResult =
        mvc.perform(
                MockMvcRequestBuilders.get(PERSONS_LIST)
                    .queryParam("firstName", "Phani")
                    .queryParam("lastName", "Yallam")
                    .contentType(MediaType.APPLICATION_JSON))
            .andReturn();
    response = mvcResult.getResponse();
  }

  private void givenAPersonExists() throws Exception {
    itUtils.createPerson(personStub);
  }

  @AfterAll
  void cleanup() throws SQLException, ClassNotFoundException {
    dbUtils.cleanup();
  }
}
