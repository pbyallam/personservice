package com.cpp.personservice.integrationtests;

import com.cpp.personservice.integrationtests.utils.DbUtils;
import com.cpp.personservice.integrationtests.utils.ITUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DeletePersonByIdTest {
  private static final String PERSONS_LIST = "/persons";
  @Autowired private MockMvc mvc;
  @Autowired private ITUtils itUtils;
  @Autowired private DbUtils dbUtils;

  @Value("classpath:person1_with_same_surname.json")
  private Resource person1WithSameSurnameStub;

  private String createdPersonId;
  private MvcResult deleteResponse;

  @Test
  void shouldBeAbleToDeletePersonRecordById() throws Exception {
    givenAPersonExists();
    whenPersonIsDeleted();
    thenResponseStatusShouldBe200();
    thenPersonShouldBeDeletedInDB();
  }

  private void whenPersonIsDeleted() throws Exception {
    deleteResponse =
        mvc.perform(
                MockMvcRequestBuilders.delete(PERSONS_LIST)
                    .queryParam("id", createdPersonId)
                    .contentType(MediaType.APPLICATION_JSON))
            .andReturn();
  }

  private void givenAPersonExists() throws Exception {
    MvcResult createPersonResponse = itUtils.createPerson(person1WithSameSurnameStub);
    createdPersonId = createPersonResponse.getResponse().getContentAsString();
  }

  private void thenResponseStatusShouldBe200() {
    assertEquals(HttpStatus.OK.value(), deleteResponse.getResponse().getStatus());
  }

  private void thenPersonShouldBeDeletedInDB() {
    try (Statement stmt = dbUtils.getConnection().createStatement()) {
      String sql = "select * from PERSON_TBL where PERSON_TBL.id=" + createdPersonId;

      ResultSet rs = stmt.executeQuery(sql);
      assertFalse(
          rs.isBeforeFirst(), "person should have been deleted by this id:" + createdPersonId);
    } catch (Exception se) {
      se.printStackTrace();
      fail("unable to retrieve data from db");
    }
  }

  @AfterAll
  void cleanup() throws SQLException, ClassNotFoundException {
    dbUtils.cleanup();
  }
}
