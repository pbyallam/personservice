package com.cpp.personservice.integrationtests;

import com.cpp.personservice.integrationtests.utils.DbUtils;
import com.cpp.personservice.integrationtests.utils.ITUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;
import java.sql.SQLException;

import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GetPersonListByLastNameTest {
  private static final String PERSONS_LIST = "/persons";
  private static String LAST_NAME = "Yallam";

  @Autowired private MockMvc mvc;
  @Autowired private ITUtils itUtils;
  @Autowired private DbUtils dbUtils;

  @Value("classpath:person_list.json")
  private Resource expectedPersonsResource;

  @Value("classpath:person1_with_same_surname.json")
  private Resource person1WithSameSurnameStub;

  @Value("classpath:person2_with_same_surname.json")
  private Resource person2WithSameSurnameStub;

  @Value("classpath:person3_with_different_surname.json")
  private Resource person3WithDifferentSurnameStub;

  private MvcResult mvcResult;

  @Test
  void shouldBeAbleToReturnListOfPersonsBasedOnLastName() throws Exception {

    givenSomePersonsExistWithSameLastName();
    givenAPersonExistWithDifferentLastName();

    whenPersonsAreRetrievedByLastName();
    thenResponseCodeShouldBe200();
    thenResponseJsonShouldOnlyContainPersonsWithMatchedLastName();
  }

  private void thenResponseJsonShouldOnlyContainPersonsWithMatchedLastName() throws IOException {
    assertThatJson(mvcResult.getResponse().getContentAsString())
        .isEqualTo(itUtils.getResourceText(expectedPersonsResource));
  }

  private void thenResponseCodeShouldBe200() {
    assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
  }

  private void whenPersonsAreRetrievedByLastName() throws Exception {
    mvcResult =
        mvc.perform(
                MockMvcRequestBuilders.get(PERSONS_LIST)
                    .queryParam("lastName", LAST_NAME)
                    .contentType(MediaType.APPLICATION_JSON))
            .andReturn();
  }

  private void givenAPersonExistWithDifferentLastName() throws Exception {
    itUtils.createPerson(person3WithDifferentSurnameStub);
  }

  private void givenSomePersonsExistWithSameLastName() throws Exception {
    itUtils.createPerson(person1WithSameSurnameStub);
    itUtils.createPerson(person2WithSameSurnameStub);
  }

  @AfterAll
  void cleanup() throws SQLException, ClassNotFoundException {
    dbUtils.cleanup();
  }
}
