package com.cpp.personservice.integrationtests;

import com.cpp.personservice.integrationtests.utils.DbUtils;
import com.cpp.personservice.integrationtests.utils.ITUtils;
import net.javacrumbs.jsonunit.core.Option;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;

import java.io.IOException;
import java.sql.SQLException;

import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreatePersonValidationsTest {
  @Autowired private ITUtils itUtils;
  @Autowired private DbUtils dbUtils;

  @Value("classpath:person_with_future_dob.json")
  private Resource personStubWithFutureDOB;

  @Value("classpath:expected_dob_validation_failure.json")
  private Resource expectedDOBValidationFailure;

  @Value("classpath:person_with_missing_person_mandatory_fields.json")
  private Resource personStubWithMissingMandatoryFields;

  @Value("classpath:person_with_missing_address_mandatory_fields.json")
  private Resource personStubWithMissingAddressFields;

  @Value("classpath:expected_person_mandatory_fields_validation_failure.json")
  private Resource expectedMandatoryFieldsValidationFailure;

  @Value("classpath:expected_address_mandatory_fields_validation_failure.json")
  private Resource expectedAddressMandatoryFieldsValidationFailure;

  private MockHttpServletResponse response;

  @Test
  void shouldFailToCreateAPersonForFutureDOB() throws Exception {
    whenAPersonIsCreatedWithFutureDOB();
    thenResponseStatusShouldBe400();
    thenResponseShouldHaveDOBValidationError();
  }

  @Test
  void shouldFailToCreateAPersonForMissingMandatoryFields() throws Exception {
    whenAPersonIsCreatedWithMissingMandatoryFields();
    thenResponseStatusShouldBe400();
    thenResponseShouldHavePersonMandatoryFieldsValidationError();
  }

  @Test
  void shouldFailToCreateAddressForMissingMandatoryFields() throws Exception {
    whenAddressIsCreatedWithMissingMandatoryFields();
    thenResponseStatusShouldBe400();
    thenResponseShouldHaveAddressMandatoryFieldsValidationError();
  }

  private void whenAddressIsCreatedWithMissingMandatoryFields() throws Exception {
    MvcResult mvcResult = itUtils.createPerson(personStubWithMissingAddressFields);
    response = mvcResult.getResponse();
  }

  private void thenResponseStatusShouldBe400() {
    assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
  }

  private void thenResponseShouldHaveDOBValidationError() throws IOException {
    assertThatJson(response.getContentAsString())
        .isEqualTo(itUtils.getResourceText(expectedDOBValidationFailure));
  }

  private void thenResponseShouldHavePersonMandatoryFieldsValidationError() throws IOException {
    assertThatJson(response.getContentAsString())
        .when(Option.IGNORING_ARRAY_ORDER).isArray()
        .isEqualTo(itUtils.getResourceText(expectedMandatoryFieldsValidationFailure));
  }

  private void thenResponseShouldHaveAddressMandatoryFieldsValidationError() throws IOException {
    assertThatJson(response.getContentAsString())
        .when(Option.IGNORING_ARRAY_ORDER).isArray()
        .isEqualTo(itUtils.getResourceText(expectedAddressMandatoryFieldsValidationFailure));
  }

  private void whenAPersonIsCreatedWithMissingMandatoryFields() throws Exception {
    MvcResult mvcResult = itUtils.createPerson(personStubWithMissingMandatoryFields);
    response = mvcResult.getResponse();
  }

  private void whenAPersonIsCreatedWithFutureDOB() throws Exception {
    MvcResult mvcResult = itUtils.createPerson(personStubWithFutureDOB);
    response = mvcResult.getResponse();
  }

  @AfterAll
  void cleanup() throws SQLException, ClassNotFoundException {
    dbUtils.cleanup();
  }
}
